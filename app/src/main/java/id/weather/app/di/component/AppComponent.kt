package id.weather.app.di.component

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import id.weather.app.MyApplication
import id.weather.app.di.builder.ActivityBuilder
import id.weather.app.di.builder.FragmentBuilder
import id.weather.app.di.module.AppModule
import id.weather.app.di.module.NetworkModule
import id.weather.app.di.source.LocalDataSourceModule
import id.weather.app.di.source.RemoteDataSourceModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class, AppModule::class, NetworkModule::class,
        RemoteDataSourceModule::class, LocalDataSourceModule::class,
        ActivityBuilder::class, FragmentBuilder::class
    ]
)
interface AppComponent : AndroidInjector<MyApplication> {

    @Component.Factory
    abstract class Factory : AndroidInjector.Factory<MyApplication>

}