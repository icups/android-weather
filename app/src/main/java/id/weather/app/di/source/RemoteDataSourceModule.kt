package id.weather.app.di.source

import dagger.Module
import dagger.Provides
import id.weather.services.WeatherServices
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class RemoteDataSourceModule {

    @Singleton
    @Provides
    fun provideWeatherServices(retrofit: Retrofit): WeatherServices {
        return retrofit.create(WeatherServices::class.java)
    }

}