package id.weather.app.di.builder

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.weather.app.ui.blank.BlankFragment
import id.weather.app.ui.main.MainModule
import id.weather.app.ui.permission.LocationPermissionFragment
import id.weather.app.ui.weather.WeatherFragment
import id.weather.app.ui.weather.WeatherModule

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun contributeBlankFragment(): BlankFragment

    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun contributeLocationPermissionFragment(): LocationPermissionFragment

    @ContributesAndroidInjector(modules = [WeatherModule::class])
    abstract fun contributeWeatherFragment(): WeatherFragment

}