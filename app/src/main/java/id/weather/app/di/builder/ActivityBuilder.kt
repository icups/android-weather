package id.weather.app.di.builder

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.weather.app.ui.main.MainActivity
import id.weather.app.ui.main.MainModule

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun contributeMainActivity(): MainActivity

}