package id.weather.app.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import id.weather.app.MyApplication
import id.weather.app.shared.AppPreferences
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideContext(application: MyApplication): Context {
        return application
    }

    @Singleton
    @Provides
    fun providePreferences(context: Context): AppPreferences {
        return AppPreferences(context)
    }

}