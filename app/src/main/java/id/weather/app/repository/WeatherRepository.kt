package id.weather.app.repository

import id.weather.app.BuildConfig
import id.weather.model.WeatherResponse
import id.weather.services.WeatherServices
import javax.inject.Inject

class WeatherRepository @Inject constructor(private val remoteDataSource: WeatherServices) {

    suspend fun getWeatherByCityId(cityId: String): WeatherResponse {
        return remoteDataSource.getWeatherByCityId(BuildConfig.API_KEY, cityId)
    }

    suspend fun getWeatherByCoordinate(lat: String, lng: String): WeatherResponse {
        return remoteDataSource.getWeatherByCoordinate(BuildConfig.API_KEY, lat, lng)
    }

}