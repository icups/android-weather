package id.weather.app.binding

import android.graphics.Bitmap
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import id.weather.ext.image.loadImage
import id.weather.ext.resource.getResId

object ImageBinding {

    @JvmStatic
    @BindingAdapter("bind:loadBitmap")
    fun loadBitmap(view: ImageView?, bitmap: Bitmap?) {
        bitmap?.run { view?.setImageBitmap(this) }
    }

    @JvmStatic
    @BindingAdapter("bind:loadImage")
    fun loadImageFromUrl(view: ImageView?, url: String?) {
        view?.loadImage(url)
    }

    @JvmStatic
    @BindingAdapter("bind:loadResource")
    fun loadImageResource(view: ImageView?, resName: String?) {
        if (resName == null || view == null) return
        view.run { loadImage(context.getResId(resName)) }
    }

}