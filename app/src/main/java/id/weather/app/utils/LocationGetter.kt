package id.weather.app.utils

import android.annotation.SuppressLint
import android.content.Context
import android.location.LocationManager
import androidx.core.location.LocationManagerCompat
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import id.weather.ext.context.getLocationManager
import id.weather.ext.context.onLocationPermitted

class LocationGetter(
    private val context: Context,
    private val onPermissionGranted: (latitude: Double, longitude: Double) -> Unit
) {

    // To get current location
    private lateinit var locationClient: FusedLocationProviderClient

    // To set location permission
    private val builder = LocationSettingsRequest.Builder()

    private var latitude: Double = 0.0
    private var longitude: Double = 0.0

    init {
        setupLocation()

        onLocationFetched {
            onPermissionGranted(latitude, longitude)
        }
    }

    private inline fun onLocationFetched(action: () -> Unit) {
        if (isLocationAccessDisabled()) {
            createAndCheckLocationRequest()
        } else {
            context.onLocationPermitted {
                fetchCurrentLocation()
                action()
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun requestLocationUpdate() {
        context.onLocationPermitted {
            locationClient.requestLocationUpdates(getLocationRequest(), locationCallback, context.mainLooper)
        }
    }

    private fun setupLocation() {
        locationClient = LocationServices.getFusedLocationProviderClient(context)

        builder.addLocationRequest(getLocationRequest())
        builder.setAlwaysShow(true)
    }

    private fun getLocationRequest() = LocationRequest.create().apply {
        interval = 10000
        fastestInterval = 5000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    @SuppressLint("MissingPermission")
    private fun fetchCurrentLocation() {
        context.getLocationManager()?.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)?.let { location ->
            latitude = location.latitude
            longitude = location.longitude
        }
    }

    private var locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            if (locationResult == null) {
                return
            }

            for (location in locationResult.locations) {
                context.onLocationPermitted {
                    onPermissionGranted(location.latitude, location.longitude)
                }
            }

            locationClient.removeLocationUpdates(this)
        }
    }.also { locationCallback = it }

    private fun createAndCheckLocationRequest() {
        val task: Task<LocationSettingsResponse> = LocationServices.getSettingsClient(context).checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            requestLocationUpdate()
        }
    }

    private fun isLocationAccessDisabled(): Boolean {
        return context.getLocationManager()?.run { !LocationManagerCompat.isLocationEnabled(this) } ?: false
    }

}