package id.weather.app.ui.blank

import id.weather.app.R
import id.weather.app.base.BaseFragment
import id.weather.app.databinding.FragmentBlankBinding
import id.weather.app.ui.main.MainViewModel
import id.weather.ext.context.onLocationPermitted
import id.weather.ext.navigation.navigateTo

class BlankFragment : BaseFragment<MainViewModel, FragmentBlankBinding>(MainViewModel::class.java) {

    override fun onViewCreated() {
        binding.apply {
            lifecycleOwner = this@BlankFragment
        }

        initializeFragment {
            navController.navigateTo(R.id.startLocationPermissionFragment)
        }
    }

    override fun layoutResources(): Int {
        return R.layout.fragment_blank
    }

    private inline fun initializeFragment(exception: () -> Unit) {
        context?.run {
            onLocationPermitted {
                navController.navigateTo(R.id.startWeatherFragment)
                return
            }
        }
        exception.invoke()
    }

}