package id.weather.app.ui.main

import android.app.Activity
import android.content.Intent
import id.weather.app.R
import id.weather.app.base.BaseActivity
import id.weather.app.databinding.ActivityMainBinding
import java.util.*

class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>(MainViewModel::class.java) {

    companion object {
        @JvmStatic
        fun start(context: Activity) {
            val starter = Intent(context, MainActivity::class.java)
            context.startActivity(starter)
            context.finishAffinity()
        }
    }

    override fun onViewCreated() {
        binding.apply {
            lifecycleOwner = this@MainActivity
            vm = viewModel
        }
    }

    override fun layoutResources(): Int {
        return R.layout.activity_main
    }

    override fun fullStatusBar(): Boolean {
        return true
    }

}