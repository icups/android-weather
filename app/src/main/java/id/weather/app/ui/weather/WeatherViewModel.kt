package id.weather.app.ui.weather

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.weather.app.base.BaseViewModel
import id.weather.app.repository.WeatherRepository
import id.weather.model.State
import id.weather.model.WeatherResponse
import kotlinx.coroutines.launch
import java.net.UnknownHostException
import javax.inject.Inject

class WeatherViewModel @Inject constructor(private val repository: WeatherRepository) : BaseViewModel() {

    enum class UiRequest { RETRY, SEARCH, DETAIL, REFRESH, SETTING, DONE }
    data class Parcel(val view: View? = null)

    private val mUiRequest = MutableLiveData<Pair<UiRequest, Parcel>>()
    val uiRequest: LiveData<Pair<UiRequest, Parcel>> = mUiRequest

    private val mWeather = MutableLiveData<State<WeatherResponse>>()
    val weather: LiveData<State<WeatherResponse>> = mWeather

    fun getWeatherByCoordinate(lat: String, lng: String) {
        viewModelScope.launch {
            try {
                mWeather.postValue(State.Loading)
                repository.getWeatherByCoordinate(lat, lng).run {
                    mWeather.postValue(State.Success(this))
                    mUiMode.postValue(UiMode.SUCCESS)
                }
            } catch (e: Exception) {
                mWeather.postValue(
                    when (e) {
                        is UnknownHostException -> State.Failure("Please check your internet connection and try again.")
                        else -> State.Failure(e.message)
                    }
                )
                mUiMode.postValue(UiMode.ERROR)
                e.printStackTrace()
            }
            mUiRequest.postValue(UiRequest.DONE to Parcel())
        }
    }

    fun clickRetry() {
        mUiRequest.postValue(UiRequest.RETRY to Parcel())
    }

    fun clickSearch() {
        mUiRequest.postValue(UiRequest.SEARCH to Parcel())
    }

    fun clickDetail() {
        mUiRequest.postValue(UiRequest.DETAIL to Parcel())
    }

    fun clickRefresh() {
        mUiRequest.postValue(UiRequest.REFRESH to Parcel())
    }

    fun clickSetting() {
        mUiRequest.postValue(UiRequest.SETTING to Parcel())
    }

}