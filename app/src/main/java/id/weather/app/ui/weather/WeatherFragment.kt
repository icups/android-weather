package id.weather.app.ui.weather

import android.annotation.SuppressLint
import androidx.core.view.isVisible
import id.weather.app.R
import id.weather.app.base.BaseFragment
import id.weather.app.databinding.FragmentWeatherBinding
import id.weather.app.ui.weather.WeatherViewModel.UiRequest
import id.weather.app.utils.LocationGetter
import id.weather.ext.alert.showToast
import id.weather.ext.common.launchDelayedFunction
import id.weather.ext.observer.observe
import id.weather.ext.view.rotation
import id.weather.model.State
import id.weather.model.WeatherResponse
import java.text.SimpleDateFormat
import java.util.*

class WeatherFragment : BaseFragment<WeatherViewModel, FragmentWeatherBinding>(WeatherViewModel::class.java) {

    private var latitude = ""
    private var longitude = ""

    override fun onViewCreated() {
        binding.apply {
            lifecycleOwner = this@WeatherFragment
            vm = viewModel
        }
    }

    override fun layoutResources(): Int {
        return R.layout.fragment_weather
    }

    override fun onBackPressed() {
        finish()
    }

    override fun fullStatusBar(): Boolean {
        return true
    }

    @SuppressLint("MissingPermission")
    override fun initAPI() {
        context?.run {
            LocationGetter(this) { lat, lng ->
                latitude = lat.toString(); longitude = lng.toString()
                viewModel.getWeatherByCoordinate(latitude, longitude)
            }
        }
    }

    override fun setupObserver() {
        viewModel.run {
            observe(uiRequest) {
                animateButton(it.first)

                when (it.first) {
                    UiRequest.SEARCH, UiRequest.DETAIL, UiRequest.SETTING -> showToast("Under development")
                    UiRequest.RETRY, UiRequest.REFRESH -> getWeatherByCoordinate(latitude, longitude)
                    UiRequest.DONE -> onActionDone()
                }
            }

            observe(weather) {
                when (it) {
                    is State.Loading -> inProcess = true
                    is State.Success -> assignWeather(it.data)
                    else -> return@observe
                }
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun assignWeather(response: WeatherResponse) {
        val countryId = response.country?.id.orEmpty()
        val countryName = Locale("", countryId).displayCountry

        val dateFormat = SimpleDateFormat("E\ndd")
        val currentDate = dateFormat.format(Date())

        binding.run {
            weather = response.weather.firstOrNull()
            main = response.main
            city = response.city
            country = countryName
            dateTime = "$currentDate'"
        }
    }

    private fun animateButton(id: UiRequest) {
        binding.run {
            if (id == UiRequest.RETRY) imageRetry.rotation(180f)
            else if (id == UiRequest.REFRESH) buttonRefresh.rotation()
        }
    }

    private fun onActionDone() {
        inProcess = false

        launchDelayedFunction(2000) {
            binding.run {
                imageRetry.clearAnimation()
                buttonRefresh.clearAnimation()
            }
        }

        binding.placeholder.root.isVisible = false
    }

}