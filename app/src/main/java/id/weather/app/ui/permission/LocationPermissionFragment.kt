package id.weather.app.ui.permission

import android.Manifest
import androidx.activity.result.contract.ActivityResultContracts
import id.weather.app.R
import id.weather.app.base.BaseFragment
import id.weather.app.databinding.FragmentLocationPermissionBinding
import id.weather.app.ui.main.MainViewModel
import id.weather.app.ui.main.MainViewModel.UiRequest
import id.weather.ext.alert.showToast
import id.weather.ext.navigation.navigateTo
import id.weather.ext.observer.observe
import id.weather.ext.property.orFalse

class LocationPermissionFragment : BaseFragment<MainViewModel, FragmentLocationPermissionBinding>(MainViewModel::class.java) {

    private val requestMultiplePermissions =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            val fineLocation = permissions[Manifest.permission.ACCESS_FINE_LOCATION].orFalse()
            val coarseLocation = permissions[Manifest.permission.ACCESS_COARSE_LOCATION].orFalse()

            if (fineLocation && coarseLocation) navController.navigateTo(R.id.startWeatherFragment)
            else showToast("Access denied")
        }

    override fun onViewCreated() {
        binding.apply {
            lifecycleOwner = this@LocationPermissionFragment
            vm = viewModel
        }
    }

    override fun layoutResources(): Int {
        return R.layout.fragment_location_permission
    }

    override fun fullStatusBar(): Boolean {
        return true
    }

    override fun onBackPressed() {
        finish()
    }

    override fun setupObserver() {
        viewModel.run {
            observe(uiRequest) {
                when (it.first) {
                    UiRequest.PERMISSION -> requestMultiplePermissions.launch(
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                    )
                }
            }
        }
    }

}