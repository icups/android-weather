package id.weather.app.ui.weather

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import id.weather.app.di.ViewModelProviderFactory
import id.weather.app.repository.WeatherRepository

@Module
class WeatherModule {

    @Provides
    fun provideWeatherViewModel(repository: WeatherRepository): WeatherViewModel {
        return WeatherViewModel(repository)
    }

    @Provides
    fun provideWeatherViewModelFactory(viewModel: WeatherViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(viewModel)
    }

}