package id.weather.app.ui.main

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import id.weather.app.base.BaseViewModel

class MainViewModel : BaseViewModel() {

    enum class UiRequest { PERMISSION }
    data class Parcel(val view: View? = null)

    private val mUiRequest = MutableLiveData<Pair<UiRequest, Parcel>>()
    val uiRequest: LiveData<Pair<UiRequest, Parcel>> = mUiRequest

    fun clickRequestPermission() {
        mUiRequest.postValue(UiRequest.PERMISSION to Parcel())
    }

}