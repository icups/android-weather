package id.weather.app.constant

import id.weather.app.R

object Nav {

    const val Weather: Int = R.id.nav_host_weather

}