package id.weather.ext.widget

import android.graphics.Color
import androidx.cardview.widget.CardView
import id.weather.ext.view.visible

fun CardView?.backgroundColor(hexCode: String) {
    this?.run {
        setCardBackgroundColor(Color.parseColor(hexCode))
        visible(false)
    }
}