package id.weather.ext.log

import android.util.Log
import id.weather.app.BuildConfig

fun logException(e: Exception) {
    if (BuildConfig.DEBUG) Log.e("WeatherApp-Exception >", "Result > ${e.message}")
    e.printStackTrace()
}

fun logError(msg: String?) {
    if (BuildConfig.DEBUG) Log.e("WeatherApp-Error >", "Result > $msg")
}

fun logSuccess(msg: String?) {
    if (BuildConfig.DEBUG) Log.i("WeatherApp-Success", "Result > $msg")
}

fun logInfo(msg: String?) {
    if (BuildConfig.DEBUG) Log.i("WeatherApp-Info >", "Result > $msg")
}