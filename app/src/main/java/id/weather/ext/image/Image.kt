package id.weather.ext.image

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import id.weather.app.R
import id.weather.ext.resource.getDrawableResource

fun ImageView.loadImage(url: String?) {
    if (url != null && url.isEmpty()) return
    Glide.with(this)
        .load(url)
        .transition(DrawableTransitionOptions.withCrossFade())
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .placeholder(R.color.colorTransparent)
        .error(R.color.colorTransparent)
        .into(this)
}

fun ImageView.loadImage(@DrawableRes resId: Int?) {
    if (resId == null || resId == tag) return

    Glide.with(this)
        .load(context.getDrawableResource(resId))
        .transition(DrawableTransitionOptions.withCrossFade())
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .placeholder(R.color.colorTransparent)
        .error(R.color.colorTransparent)
        .into(this)

    tag = resId
}