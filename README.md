# android-weather

![UI-Design](https://cdn.dribbble.com/users/6308517/screenshots/15058067/media/167a5ea4936b87f5c2a8f5c37da26dd3.png?compress=1&resize=1600x1200)
Weather apps has local and international weather forecasts from the most accurate weather forecasting technology featuring up to the minute weather reports.

### Tech stack
![MVVM](https://cdn-images-1.medium.com/fit/t/1600/480/1*kWwjlkOEyTV6M7W7tZrs1w.png)
*  **Kotlin** as Programming Language of the project
*  **MVVM** as Architecture Code of the project
*  **Dagger 2** as Dependency Injection of the project
*  **Coroutines** to Execute function asynchronously

### UI References
*  [Dribble](https://dribbble.com/shots/15058067-Weather-App-UI-Design)
*  [Freepik](https://www.freepik.com/popular-vectors)
*  [Lottie](https://lottiefiles.com/featured)
*  [Pinterest](https://id.pinterest.com/)
