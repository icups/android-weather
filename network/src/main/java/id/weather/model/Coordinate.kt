package id.weather.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Coordinate(
    @SerializedName("lon") val longitude: String,
    @SerializedName("lat") val latitude: String
) : Parcelable