package id.weather.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class WeatherResponse(
    val id: String = "",
    @SerializedName("name") val city: String = "",
    @SerializedName("coord") val coordinate: Coordinate? = null,
    val weather: List<Weather> = emptyList(),
    val main: Main? = null,
    @SerializedName("sys") val country: Country? = null
) : Parcelable