package id.weather.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.weather.repository.BuildConfig
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class Weather(
    val id: Int,
    @SerializedName("main") val status: String,
    val description: String,
    val icon: String
) : Parcelable {

    fun getBackgroundResource(): String {
        return when (id) {
            in 200..232 -> "img_weather_thunderstorm"
            in 300..531 -> "img_weather_rain"
            in 600..622 -> "img_weather_snow"
            in 701..781 -> "img_weather_fog"
            800 -> "img_weather_clear_sky"
            else -> "img_weather_cloudy"
        }
    }

    fun getIconUrl(): String = "${BuildConfig.URL_IMAGE}$icon@4x.png"
    fun getFormattedDescription(): String = description.capitalize(Locale.getDefault())

}