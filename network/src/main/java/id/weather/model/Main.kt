package id.weather.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlin.math.roundToInt

@Parcelize
data class Main(val temp: Double) : Parcelable {
    fun temperature(): String = kotlin.math.ceil(temp - 273.15).roundToInt().toString()
}