package id.weather.services

import id.weather.model.WeatherResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherServices {

    @GET("weather")
    suspend fun getWeatherByCityId(
        @Query("appid") apiKey: String,
        @Query("id") id: String
    ): WeatherResponse

    @GET("weather")
    suspend fun getWeatherByCoordinate(
        @Query("appid") apiKey: String,
        @Query("lat") lat: String,
        @Query("lon") lng: String
    ): WeatherResponse

}