package id.weather.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "user")
class UserEntity(
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    @ColumnInfo(name = "profile_id") var profileId: String = "",
    @ColumnInfo(name = "full_name") var fullName: String = "User",
    var email: String = "",
    @ColumnInfo(name = "phone_number") var phoneNumber: String = "",
    @ColumnInfo(name = "avatar_url") var avatarUrl: String = "",
    var balance: Int = 0
) : Parcelable