package id.weather.database

import androidx.room.Database
import androidx.room.RoomDatabase
import id.weather.dao.UserDao
import id.weather.entity.UserEntity

@Database(entities = [UserEntity::class], version = 1, exportSchema = false)
abstract class UserDatabase : RoomDatabase() {
    abstract fun dao(): UserDao
}